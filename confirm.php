<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet prefetch" href="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <title>Đăng kí sinh viên</title>
    <style>
        #main {
            width: 550px;
            border: 2px solid #4d7aa2;
            border-radius: 8px;
            padding: 32px 80px;
            display: block;
            margin: auto;
        }

        #main .wrapper {

            width: 100%;

            display: flex;
            align-items: center;
            flex-direction: column;
        }

        .wrapper .showError {

            width: 100%;
        }


        .formDisplay {
            width: 100%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            margin-top: 16px;
            /* margin-bottom: 32px; */
        }

        .form-group {
            width: 100%;
            display: flex;
            margin-bottom: 16px;

        }

        .form-group .form-label {
            align-items: center;
            display: flex;
            justify-content: center;
            width: 30%;
            max-height: 40px;
            text-align: center;
            background-color: #70ad47;
            padding: 8px 12px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;
            margin-right: 16px;
            margin-bottom: 0;
            color: #fff;


        }

        .form-group .form-label .required {
            color: red;
            margin-left: 4px;
        }


        .form-group .form-input {
            width: 70%;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: row;
            flex: 1 1 0;

        }

        .form-group .form-input .studentImg {
            width: 64px;
        }



        .form-group .form-input input[type="text"] {
            width: 100%;
            font-size: 20px;
            padding: 8px 4px;
            font-weight: 500;

        }

        .form-group .form-input p {
            text-align: left;
        }

        #datepicker {
            width: 180px;
            margin: 0 20px 20px 20px;
        }

        #datepicker>span:hover {
            cursor: pointer;
        }

        .form-group .form-input input[type="radio"] {
            /* width: 30%; */
            /* font-size: 12px; */

            border: 1px solid #4d7aa2;
            background-color: #5b9bd5;
            width: 16px;
        }

        .form-group .form-input>p {
            margin: 0;
            font-weight: 600;
            font-size: 20px;
        }

        .form-input input {
            border: none;
            outline: none;
        }

        .form-group .form-input input[type="radio"]:hover {
            cursor: pointer;
            background-color: #5b9bd5;
        }

        .form-group .form-input .radio-label {
            margin-left: 8px;
            margin-bottom: 0;
            font-weight: 500;
        }

        .form-group .form-input #falcuties {
            width: 100%;
            font-size: 20px;
            padding: 10px 4px;
            border: 2px solid #4d7aa2;
            border-radius: 2px;

        }

        select {
            background: url("data:image/svg+xml,<svg height='20px' width='20px' viewBox='0 0 16 16' fill='%23000000' xmlns='http://www.w3.org/2000/svg'><path d='M7.247 11.14 2.451 5.658C1.885 5.013 2.345 4 3.204 4h9.592a1 1 0 0 1 .753 1.659l-4.796 5.48a1 1 0 0 1-1.506 0z'/></svg>") no-repeat;
            background-position: calc(100% - 0.75rem) center !important;
            -moz-appearance: none !important;
            -webkit-appearance: none !important;
            appearance: none !important;
            padding-right: 2rem !important;
        }

        .form-group .form-input .radio-group {
            display: flex;
            flex-direction: row;
            margin-right: 24px;
        }

        .form-group .form-input .radio-group label {
            font-size: 20px;
        }

        .formDisplay .submit-btn {
            text-align: center;
            width: 120px;
            color: #fff;
            background-color: #70ad47;
            font-size: 14px;
            padding: 12px;
            border: 2px solid #4d7aa2;
            border-radius: 6px;
            cursor: pointer;
        }
    </style>
</head>

<body>
    <?php
    ?>
    <div id="main">
        <div class="wrapper">
            <div class="showError">
            </div>
            <form class="formDisplay" method="POST" action="#">
                <div class="form-group">
                    <label class="form-label">Họ và tên</label>
                    <div class="form-input">
                        <?php
                        echo '<p >' . $_SESSION["name"] . '</p>'; ?>
                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Giới tính</label>
                    <div class="form-input">

                        <?php

                        if ($_SESSION["gender"] == '0') {

                            echo '<p >Nam</p>';
                        } else {
                            echo '<p >Nữ</p>';
                        }


                        ?>

                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Phân khoa</label>
                    <div class="form-input">
                        <?php

                        if ($_SESSION["falcuty"] == 'MAT') {
                            echo '<p >Khoa học máy tính</p>';
                        } else if ($_SESSION["falcuty"] == 'KHD') {
                            echo '<p >Khoa học dữ liệu</p>';
                        }
                        ?>
                    </div>


                </div>
                <div class="form-group">
                    <label class="form-label">Ngày sinh</label>
                    <div class="form-input">
                        <?php echo '<p>' . $_SESSION["dob"] . '</p>';
                        ?>

                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Địa chỉ</label>
                    <div class="form-input">
                        <?php
                        echo '<p >' . $_SESSION["address"] . '</p>';
                        ?>
                    </div>

                </div>
                <div class="form-group">
                    <label class="form-label">Hình ảnh</label>
                    <div class="form-input">
                        <?php
                        if (!empty($_SESSION["image"])) {
                            echo '<img class="studentImg" src="' . $_SESSION["image"] . '" alt="Student avatar">';
                        }
                        ?>
                    </div>

                </div>
                <input type="submit" class="submit-btn" name="submit" value="Xác Nhận">

            </form>
        </div>
    </div>

</body>

<?php
$servername = "localhost";
$username = "root";
$password = "";
$db = 'qlsv';


// Create connection
$conn = mysqli_connect($servername, $username, $password, $db);

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
mysqli_set_charset($mysqli, 'utf8mb4');

printf("Success... %s\n", mysqli_get_host_info($mysqli));
echo "Connected successfully";
if (isset($_POST['submit'])) {
    $name = $_SESSION['name'];
    $gender = $_SESSION['gender'];
    $falcuty = $_SESSION['falcuty'];
    $birthday = $_SESSION['dob'];
    $address = $_SESSION['address'];
    $avatar = $_SESSION['image'];
    $sql = "INSERT INTO student(name,gender,falcuty,birthday,address,avatar)
            VALUES ('$name','$gender','$falcuty','$birthday','$address','$avatar')";
    $result = mysqli_query($conn, $sql);
    if ($result) {
        echo "Insert data successfully";
    } else {
        echo "Error!!!!";
    }
}
?>

</html>
